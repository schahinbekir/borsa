-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 17 Haz 2021, 16:47:24
-- Sunucu sürümü: 10.1.26-MariaDB
-- PHP Sürümü: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `borsa_projesi`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alim_islemi`
--

CREATE TABLE `alim_islemi` (
  `trunsactionId` bigint(20) NOT NULL,
  `buyerUserId` int(11) NOT NULL,
  `sellerUserId` int(11) NOT NULL,
  `productName` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productAmount` double(60,2) NOT NULL,
  `productUnit` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productPrice` double(60,2) NOT NULL,
  `transactionCreateTime` datetime NOT NULL,
  `transactionProcessTime` datetime NOT NULL,
  `productIsAdded` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alis_listesi`
--

CREATE TABLE `alis_listesi` (
  `transactionId` bigint(20) NOT NULL,
  `userId` int(11) NOT NULL,
  `productName` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productAmount` double(60,2) NOT NULL,
  `productRemainderAmount` double(60,2) NOT NULL,
  `productUnit` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productPrice` double(60,2) NOT NULL,
  `transactionCreateTime` datetime NOT NULL,
  `transactionProcessTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanicilar`
--

CREATE TABLE `kullanicilar` (
  `userId` int(11) NOT NULL,
  `userCreateTime` datetime NOT NULL,
  `userType` int(11) NOT NULL,
  `userFirstName` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userSurname` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userName` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userPassword` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userTcNumber` varchar(11) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userPhoneNumber` varchar(13) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userEmail` varchar(127) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userAddress` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `userMoney` double(60,2) NOT NULL,
  `userWaitMoneyTL` double(60,2) NOT NULL,
  `userWaitMoneyUSD` double(60,2) NOT NULL,
  `userWaitMoneyEUR` double(60,2) NOT NULL,
  `userWaitMoneyGBP` double(60,2) NOT NULL,
  `userRefuseMoney` double(60,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Tablo döküm verisi `kullanicilar`
--

INSERT INTO `kullanicilar` (`userId`, `userCreateTime`, `userType`, `userFirstName`, `userSurname`, `userName`, `userPassword`, `userTcNumber`, `userPhoneNumber`, `userEmail`, `userAddress`, `userMoney`, `userWaitMoneyTL`, `userWaitMoneyUSD`, `userWaitMoneyEUR`, `userWaitMoneyGBP`, `userRefuseMoney`) VALUES
(1, '2021-01-01 00:00:01', 1, 'Admin', 'Adminastor', 'admin1', '4e89ba0f736cc148f2b8466cd5c0655c', '45546647977', '55545555558', 'admin.1@gmail.com', 'Cihangir/İstanbul', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(2, '2021-01-01 00:00:02', 3, 'Muhasebe', 'Kullanıcısı', 'muh_kul1', '9b59b763ed72976f979aa4453a0e08a6', '45455212585', '21658545588', 'muhasebe.kullanicisi@gmail.com', 'Ata Mahallesi Cihangir/İstanbul', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(3, '2021-01-02 00:00:01', 2, 'Bekir', 'Şahin', 'bekir', '322ef61706808d5d65aeb087c7324fa1', '55744155522', '50645565607', 'bkr.1@gmail.com', 'Atatürk Mahallesi', 873.95, 0.00, 0.00, 0.00, 0.00, 0.00),
(4, '2021-02-02 16:10:21', 2, 'Yakup', 'Uslu', 'yakup', '322ef61706808d5d65aeb087c7324fa1', '54551154524', '54554454587', 'yakup@gmail.com', 'Mersin', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(5, '2021-02-04 16:11:21', 2, 'Hasan', 'Sahin', 'hasan', '322ef61706808d5d65aeb087c7324fa1', '11155522555', '31255445552', 'has853@gmail.com', 'Güneykent Mahallesi Gaziantep', 4122.88, 0.00, 0.00, 0.00, 0.00, 0.00),
(6, '2021-03-17 16:13:21', 2, 'Hazal', 'Kara', 'hazal', '322ef61706808d5d65aeb087c7324fa1', '12125454545', '23255255555', 'hazal@gmail.com', 'Fethiye/Mugla', 100.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(7, '2021-03-17 16:14:21', 2, 'Aslı', 'Kaya', 'asli', '322ef61706808d5d65aeb087c7324fa1', '21125445541', '21552555555', 'asli.kaya@gmail.com', 'İstanbul mah. Sakarya', 1223.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(8, '2021-03-17 16:15:21', 2, 'Batu', 'Er', 'batu', '322ef61706808d5d65aeb087c7324fa1', '14558775666', '54455555562', 'batu@gmail.com', 'Hasan Kalyoncu sk. Elmadağ Ankara', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(9, '2021-04-11 16:16:21', 2, 'Fatmagül', 'Aygün', 'fatmagul', '322ef61706808d5d65aeb087c7324fa1', '44584878455', '23124664564', 'ayguner@gmail.com', 'Gölbaşı/Ankara', 2156.07, 0.00, 0.00, 0.00, 0.00, 0.00),
(10, '2021-04-13 16:17:21', 2, 'Güney', 'Kalas', 'guney', '322ef61706808d5d65aeb087c7324fa1', '21127455454', '32554555855', 'guney@gmail.com', 'Alibeyköy mah Alibeyköy/İstanbul', 120.27, 0.00, 0.00, 0.00, 0.00, 0.00),
(11, '2021-04-14 16:18:21', 2, 'Ecrin', 'Gök', 'ecrin', '322ef61706808d5d65aeb087c7324fa1', '22554525554', '22255255555', 'ecrin@gmail.com', 'Kızılay mah. Kızılay sk. Kızılay/Ankara', 1202.65, 0.00, 0.00, 0.00, 0.00, 0.00),
(12, '2021-04-15 16:19:21', 2, 'Yusuf', 'Ali', 'yusuf', '322ef61706808d5d65aeb087c7324fa1', '45878754545', '14554541244', 'ali.11@gmail.com', 'Alibey sk. Giresun', 120.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(13, '2021-04-16 16:25:54', 2, 'Efe', 'Şahin', 'efe', '322ef61706808d5d65aeb087c7324fa1', '44852255555', '56555458555', 'efe@gmail.com', 'ODTÜ Teknokent', 3095.80, 0.00, 0.00, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `satis_listesi`
--

CREATE TABLE `satis_listesi` (
  `transactionId` bigint(20) NOT NULL,
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productAmount` double(60,2) NOT NULL,
  `productRemainderAmount` double(60,2) NOT NULL,
  `productUnit` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productPrice` double(60,2) NOT NULL,
  `transactionCreateTime` datetime NOT NULL,
  `transactionProcessTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunler`
--

CREATE TABLE `urunler` (
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productCreateTime` datetime NOT NULL,
  `productUpdateTime` datetime NOT NULL,
  `productName` varchar(30) COLLATE utf8mb4_turkish_ci NOT NULL,
  `productAmount` double(60,2) NOT NULL,
  `productWaitAmount` double(60,2) NOT NULL,
  `productRefuseAmount` double(60,2) NOT NULL,
  `productUnit` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Tablo döküm verisi `urunler`
--

INSERT INTO `urunler` (`userId`, `productId`, `productCreateTime`, `productUpdateTime`, `productName`, `productAmount`, `productWaitAmount`, `productRefuseAmount`, `productUnit`) VALUES
(3, 1, '2021-03-11 16:59:25', '2021-03-11 16:59:40', 'Arpa', 200.00, 0.00, 0.00, 'KG'),
(3, 2, '2021-03-11 16:59:26', '2021-03-12 16:59:42', 'Nohut', 100.00, 0.00, 0.00, 'KG'),
(3, 3, '2021-03-11 16:59:28', '2021-03-13 16:59:43', 'Buğday', 50.00, 0.00, 0.00, 'KG'),
(3, 4, '2021-03-11 16:59:30', '2021-04-07 16:59:44', 'Yulaf', 55.00, 0.00, 0.00, 'KG'),
(4, 5, '2021-04-07 16:59:44', '2021-04-15 16:29:54', 'Mısır', 134.00, 0.00, 0.00, 'KG'),
(6, 6, '2021-04-16 10:29:54', '2021-04-16 16:29:54', 'Nohut', 452.00, 0.00, 0.00, 'KG'),
(7, 7, '2021-04-16 10:44:54', '2021-04-16 19:29:54', 'Kaju', 192.00, 0.00, 0.00, 'KG'),
(8, 8, '2021-04-16 21:29:54', '2021-04-16 22:29:54', 'Nohut', 545.00, 0.00, 0.00, 'KG'),
(8, 9, '2021-04-16 20:29:54', '2021-04-16 21:29:54', 'Yulaf', 42.00, 0.00, 0.00, 'KG'),
(9, 10, '2021-04-16 23:22:54', '2021-04-16 23:29:54', 'Jelibon', 800.00, 0.00, 0.00, 'KG'),
(11, 11, '2021-04-17 10:39:54', '2021-04-17 11:29:54', 'Firik', 200.00, 0.00, 0.00, 'KG'),
(12, 12, '2021-04-17 13:29:54', '2021-04-17 16:29:54', 'Buğday', 400.00, 0.00, 0.00, 'KG'),
(12, 13, '2021-04-17 14:29:54', '2021-04-18 16:29:54', 'Yulaf', 12.00, 0.00, 0.00, 'KG'),
(10, 14, '2021-04-17 00:09:54', '2021-04-17 00:29:54', 'Arpa', 400.00, 0.00, 0.00, 'KG'),
(10, 15, '2021-04-17 10:00:54', '2021-04-17 10:29:54', 'Fıstık', 400.00, 0.00, 0.00, 'KG'),
(13, 16, '2021-04-17 15:29:54', '2021-04-19 16:29:51', 'Nohut', 100.00, 0.00, 0.00, 'KG'),
(13, 17, '2021-04-17 15:39:54', '2021-04-19 16:29:54', 'Buğday', 100.00, 0.00, 0.00, 'KG');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `alim_islemi`
--
ALTER TABLE `alim_islemi`
  ADD PRIMARY KEY (`trunsactionId`);

--
-- Tablo için indeksler `alis_listesi`
--
ALTER TABLE `alis_listesi`
  ADD PRIMARY KEY (`transactionId`),
  ADD UNIQUE KEY `trunsactionId` (`transactionId`);

--
-- Tablo için indeksler `kullanicilar`
--
ALTER TABLE `kullanicilar`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userId` (`userId`),
  ADD UNIQUE KEY `userName` (`userName`),
  ADD UNIQUE KEY `userTcNumber` (`userTcNumber`),
  ADD UNIQUE KEY `userPhoneNumber` (`userPhoneNumber`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- Tablo için indeksler `satis_listesi`
--
ALTER TABLE `satis_listesi`
  ADD PRIMARY KEY (`transactionId`),
  ADD UNIQUE KEY `transactionId` (`transactionId`);

--
-- Tablo için indeksler `urunler`
--
ALTER TABLE `urunler`
  ADD PRIMARY KEY (`productId`),
  ADD UNIQUE KEY `productId` (`productId`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `alim_islemi`
--
ALTER TABLE `alim_islemi`
  MODIFY `trunsactionId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `alis_listesi`
--
ALTER TABLE `alis_listesi`
  MODIFY `transactionId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `kullanicilar`
--
ALTER TABLE `kullanicilar`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Tablo için AUTO_INCREMENT değeri `satis_listesi`
--
ALTER TABLE `satis_listesi`
  MODIFY `transactionId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `urunler`
--
ALTER TABLE `urunler`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
