var express = require('express');
var md5 = require('md5');

var jwt = require('jsonwebtoken');

let token;
const dotenv = require('dotenv');

dotenv.config({
  path: "./config/env/config.env"
});



var router = express.Router();


const { JWT_SECRET_KEY, JWT_EXPIRE, JWT_COOKIE, NODE_ENV } = process.env;

var userId = -1;
var userType = -1;
var fullName = "";

var totalUser = 0;
var totalMoney = 0;
var totalConfirmMoney = 0;
var totalNotConfirmMoney = 0;
var totalProduct = 0;

var MySql = require('sync-mysql');
const { request } = require('../app');
var conn = new MySql({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'borsa_projesi'
});

const fetch = require('node-fetch');
const xml2js = require('xml2js');
//// exel
var excel = require('excel4node');

let link2 = "https://www.tcmb.gov.tr/kurlar/today.xml";
let link1 = "https://finans.truncgil.com/today.json";

let kurlar = {usdAlis:0, usdSatis:0, eurAlis:0, eurSatis:0, gbpAlis:0, gbpSatis:0, data:0};
var KurGuncelle = function()
{
  fetch(link2)
  .then(res => res.text())
  .then(text =>  {

    xml2js.parseString(text, (err, result) => {
      if(err) {
          throw err;
      }
  
      // `result` is a JavaScript object
      // convert it to a JSON string
      const json = JSON.stringify(result, null, 4);
  
      // log JSON string
      //console.log(json);

      let obje = JSON.parse( json )
     //console.log(obje.Tarih_Date.$)
    // console.log(obje.Tarih_Date.Currency)
      for(let index=0; index<obje.Tarih_Date.Currency.length; index++)
      {
         
        if(obje.Tarih_Date.Currency[index].$.Kod == 'USD')  
        {
          kurlar.usdAlis = obje.Tarih_Date.Currency[index].ForexBuying[0];
          kurlar.usdSatis = obje.Tarih_Date.Currency[index].ForexSelling[0];
        }
        else if(obje.Tarih_Date.Currency[index].$.Kod == 'EUR')  
        {
          kurlar.eurAlis = obje.Tarih_Date.Currency[index].ForexBuying[0];
          kurlar.eurSatis = obje.Tarih_Date.Currency[index].ForexSelling[0];
        } 
        else if(obje.Tarih_Date.Currency[index].$.Kod == 'GBP')  
        {
          kurlar.gbpAlis = obje.Tarih_Date.Currency[index].ForexBuying[0];
          kurlar.gbpSatis = obje.Tarih_Date.Currency[index].ForexSelling[0];
        } 
      }
      kurlar.date = obje.Tarih_Date.$.Tarih;

      
      
    });

    
  });
}

var AlimIslemi = function () {
// satis listesi ile alis listesini kontrol et
  let alisListesi = conn.query('SELECT * FROM alis_listesi where productRemainderAmount>0');
  if(alisListesi.length>0)
  {
    let satisListesi1 = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0');
    if(satisListesi1.length>0)
    {
      console.log('LOGGED satis listesi ');
      
      for(let index=0; index<alisListesi.length; index++)
      {
        let productName = alisListesi[index].productName;
        let productPrice = alisListesi[index].productPrice;
        let productRemainderAmount = alisListesi[index].productRemainderAmount;
        let satisListesi = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0 and productPrice<='+ productPrice + ' and productName="' + productName +  '" ORDER BY productPrice DESC');
        if(satisListesi.length>0)
        {
          console.log('Alinabilecek uygun urun var');
          for(let i=0; i<satisListesi.length; i++)
          {
            let satilabilirMiktar = satisListesi[i].productRemainderAmount;
            if(productRemainderAmount <= satilabilirMiktar)
            {
              // hepsini bundan alacagiz
              // TODO:    
                       //++ Saticiya bakiye ekle
                       //++ alis listesinden kalan miktar sifirla
                       //++ satis_listesi miktar dus  
                       //++ Aliciya urun ekle
                       //++ eger daha ucuza almissak kalan bakiyeyi aliciya ekle
                       //++ muhasebeciye komisyon ekle
                       // alim_islemi tablosuna islemi ekliyoruz
              
              conn.query('UPDATE kullanicilar SET userMoney= userMoney + ' + productRemainderAmount * satisListesi[i].productPrice + ' WHERE userId=' + satisListesi[i].userId);

              conn.query('UPDATE alis_listesi SET transactionProcessTime=NOW(),	productRemainderAmount=0 WHERE transactionId=' + alisListesi[index].transactionId);

              conn.query('UPDATE satis_listesi SET transactionProcessTime=NOW(), 	productRemainderAmount= productRemainderAmount -' + productRemainderAmount + ' WHERE transactionId=' + satisListesi[i].transactionId);


              
              let productAdi = conn.query('SELECT productId, productName FROM urunler where userId = ' + alisListesi[index].userId + ' and productName = "' + productName + '"');
              if (productAdi.length > 0) {
                conn.query("UPDATE urunler SET productUpdateTime = NOW(), productAmount = productAmount + " + productRemainderAmount + " where productId =" + productAdi[0].productId);
              }
              else {
                conn.query('INSERT INTO urunler (userId, productCreateTime, productUpdateTime, productName, productAmount, productUnit) \
                    VALUES (' + alisListesi[index].userId + ', NOW(), NOW(), "' + productName + '", ' + productRemainderAmount + ', "' + alisListesi[index].productUnit + '") ');

              }


              if(productPrice > satisListesi[i].productPrice)
              {
                // daha ucuza aldigimiz miktarin komisyon ve fiyat farkini alicinin hesabina ekleyelim
                //let eklenecekBakiye = productRemainderAmount*(productPrice - satisListesi[i].productPrice)*1.01;
                let komisyon = productRemainderAmount*satisListesi[i].productPrice*0.01 ;
              
                let eklenecekBakiye = productRemainderAmount*(productPrice - satisListesi[i].productPrice);

                console.log(komisyon);
                console.log(eklenecekBakiye);
                
                conn.query('UPDATE kullanicilar SET userMoney = userMoney + ' + komisyon + ' WHERE userType=3' );
                conn.query('UPDATE kullanicilar SET userMoney = userMoney + ' + eklenecekBakiye*1.01 + ' WHERE userId=' + alisListesi[index].userId );

              }
              else if (productPrice == satisListesi[i].productPrice)
              {
                // ayni paraya alinmis demektir sadece komisyon ekle
                let komisyon = productRemainderAmount*satisListesi[i].productPrice*0.01 ;
                
                conn.query('UPDATE kullanicilar SET userMoney = userMoney + ' + komisyon + ' WHERE userType=3' );
              }



              conn.query('INSERT INTO alim_islemi (buyerUserId,	sellerUserId,	productName,	productAmount,	productUnit,	productPrice,	transactionCreateTime,	transactionProcessTime ) VALUES (' + alisListesi[index].userId  + ', ' + satisListesi[i].userId + ', "' + productName + '", ' + productRemainderAmount + ', "' + alisListesi[index].productUnit + '", ' + satisListesi[i].productPrice + ', NOW(), NOW()) ');
               //else {break;} 
              break;
            }
            else
            {
              // bir miktari burdan alacagiz
              // TODO:                              
                       //++ Saticiya bakiye ekle
                       //++ alis listesinden kalan miktar dus
                       //++ satis_listesi miktar sifirla 
                       //++ Aliciya urun ekle
                       //++ eger daha ucuza almissak alinan miktar kadarin farkini kalan bakiyeyi aliciya ekle
                       //++ muhasebeciye komisyon ekle
                       //++ alim_islemi tablosuna islemi ekliyoruz

              conn.query('UPDATE kullanicilar SET userMoney= userMoney + ' + satilabilirMiktar * satisListesi[i].productPrice + ' WHERE userId=' + satisListesi[i].userId);

              conn.query('UPDATE alis_listesi SET transactionProcessTime=NOW(),	productRemainderAmount = productRemainderAmount -' + satilabilirMiktar + ' WHERE transactionId=' + alisListesi[index].transactionId);

              conn.query('UPDATE satis_listesi SET transactionProcessTime=NOW(), 	productRemainderAmount= 0 WHERE transactionId=' + satisListesi[i].transactionId);

              let productAdi = conn.query('SELECT productId, productName FROM urunler where userId = ' + alisListesi[index].userId + ' and productName = "' + productName + '"');
              if (productAdi.length > 0) {
                conn.query("UPDATE urunler SET productUpdateTime = NOW(), productAmount = productAmount + " + satilabilirMiktar + " where productId =" + productAdi[0].productId);
              }
              else {
                conn.query('INSERT INTO urunler (userId, productCreateTime, productUpdateTime, productName, productAmount, productUnit) \
                    VALUES (' + alisListesi[index].userId + ', NOW(), NOW(), "' + productName + '", ' + satilabilirMiktar + ', "' + alisListesi[index].productUnit + '") ');
              }


              if(productPrice > satisListesi[i].productPrice)
              {
                // daha ucuza aldigimiz miktarin komisyon ve fiyat farkini alicinin hesabina ekleyelim
                //let eklenecekBakiye = satilabilirMiktar*(productPrice - satisListesi[i].productPrice)*1.01;
                let komisyon = satilabilirMiktar*satisListesi[i].productPrice*0.01 ;
                
                let eklenecekBakiye = satilabilirMiktar*productPrice - komisyon*100;

                console.log(komisyon);
                console.log(eklenecekBakiye);

                conn.query('UPDATE kullanicilar SET userMoney = userMoney + ' + komisyon + ' WHERE userType=3' );
                conn.query('UPDATE kullanicilar SET userMoney = userMoney + ' + eklenecekBakiye*1.01 + ' WHERE userId=' + alisListesi[index].userId );

              }
              else if (productPrice == satisListesi[i].productPrice)
              {
                // ayni paraya alinmis demektir sadece komisyon ekle
                let komisyon = satilabilirMiktar*satisListesi[i].productPrice*0.01 ;
                
                conn.query('UPDATE kullanicilar SET userMoney = userMoney + ' + komisyon + ' WHERE userType=3' );
              }

              conn.query('INSERT INTO alim_islemi (buyerUserId,	sellerUserId,	productName,	productAmount,	productUnit,	productPrice,	transactionCreateTime,	transactionProcessTime ) VALUES (' + alisListesi[index].userId  + ', ' + satisListesi[i].userId + ', "' + productName + '", ' + satilabilirMiktar + ', "' + alisListesi[index].productUnit + '", ' + satisListesi[i].productPrice + ', NOW(), NOW()) ');
            

              productRemainderAmount -= satilabilirMiktar;
            }
          }
        }
        else
        {
          console.log('Alinabilecek uygun urun yok');
        }
      }


    }
    else
    {
      console.log('LOGGED satis listesi bos');
    }
    console.log('LOGGED alinacak listesi');
  }
  else
  {
    console.log('LOGGED alinacak listesi bos');
  }
  
}

var createFile1 = function(bodyVal){
  // Create a new instance of a Workbook class
  var workbook = new excel.Workbook();
  // Add Worksheets to the workbook
  
  let data = {}
  // Create a reusable style
  var style1 = workbook.createStyle({
    font: {
      color: '#e95353',
      size: 15,
    }
  });
  var style = workbook.createStyle({
    font: {
      color: '#000000',
      size: 15,
    }
  });

 /* worksheet.cell(1,1).number(100).style(style);
  worksheet.cell(2,1).string(bodyVal.urunler).style(style);
  worksheet.cell(2,2).string(bodyVal.durum).style(style);
*/

  // genel bilgileri içersin ilk 2-3 satır

  // Satis    =>  İşlem Gerçekleşme Tarihi    Ürün Tipi(Adi)     Alım Birim Tutarı      Alınan Miktar
 /* worksheet.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style);
  worksheet.cell(3,2).string("Ürün Tipi(Adi)").style(style);
  worksheet.cell(3,3).string("Alım Birim Tutarı").style(style);
  worksheet.cell(3,4).string("Alınan Miktar").style(style);
// Alış     =>  İşlem Gerçekleşme Tarihi    Ürün Tipi(Adi)     Satış Birim Tutarı     Alınan Miktar
  worksheet2.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style);
  worksheet2.cell(3,2).string("Ürün Tipi(Adi)").style(style);
  worksheet2.cell(3,3).string("Satış Birim Tutarı").style(style);
  worksheet2.cell(3,4).string("Alınan Miktar").style(style);
// Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
  worksheet3.cell(3,1).string("İşlem Oluşturma Tarihi").style(style);
  worksheet3.cell(3,2).string("Ürün Tipi(Adi)").style(style);
  worksheet3.cell(3,3).string("İşlem Türü(Alış-Satış)").style(style);
  worksheet3.cell(3,4).string("Birim Tutarı").style(style);
  worksheet3.cell(3,5).string("Talep(Alış-Satış) Miktar").style(style);*/

 
  


  if(bodyVal.urunler == "hepsi")
  {
    if(bodyVal.durum == "satis")
    {
      // alim_islemi tablosunda seller id si olan
      data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where sellerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
        data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }
      
      var worksheet2 = workbook.addWorksheet('Satış İşlem Listesi');
      worksheet2.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet2.cell(1,2).date(new Date()).style(style);
      worksheet2.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet2.cell(1,5).string(fullName).style(style);

      // Alış     =>  İşlem Gerçekleşme Tarihi    Ürün Tipi(Adi)     Satış Birim Tutarı     Alınan Miktar
      worksheet2.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet2.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet2.cell(3,3).string("Satış Birim Tutarı").style(style1);
      worksheet2.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet2.cell(3,5).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionSellProduct.length; item++)
      {
        worksheet2.cell(rawNumber,1).date(data.trunsactionSellProduct[item].transactionProcessTime).style(style);
        worksheet2.cell(rawNumber,2).string(data.trunsactionSellProduct[item].productName).style(style);
        worksheet2.cell(rawNumber,3).number(data.trunsactionSellProduct[item].productPrice).style(style);
        worksheet2.cell(rawNumber,4).number(data.trunsactionSellProduct[item].productAmount).style(style);
        worksheet2.cell(rawNumber,5).string(data.trunsactionSellProduct[item].productUnit).style(style);
        rawNumber++;
      }


    }
    else if(bodyVal.durum == "alis")
    {
      // alim_islemi tablosunda buyer id si olan
      data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where buyerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
        data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }

      var worksheet = workbook.addWorksheet('Alış İşlem Listesi');
      

      worksheet.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet.cell(1,2).date(new Date()).style(style);
      worksheet.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet.cell(1,5).string(fullName).style(style);
      
      worksheet.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet.cell(3,3).string("Alım Birim Tutarı").style(style1);
      worksheet.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet.cell(3,5).string("Ürün Birimi").style(style1);

      let rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyProduct.length; item++)
      {
        worksheet.cell(rawNumber,1).date(data.trunsactionBuyProduct[item].transactionProcessTime).style(style);
        worksheet.cell(rawNumber,2).string(data.trunsactionBuyProduct[item].productName).style(style);
        worksheet.cell(rawNumber,3).number(data.trunsactionBuyProduct[item].productPrice).style(style);
        worksheet.cell(rawNumber,4).number(data.trunsactionBuyProduct[item].productAmount).style(style);
        worksheet.cell(rawNumber,5).string(data.trunsactionBuyProduct[item].productUnit).style(style);
        rawNumber++;
      }
    }
    else if(bodyVal.durum == "bekleyen")
    {
      // alis_listesi ve satis_listesindeki productRemainderAmount>0 ve userId olan
      data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');

      data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');

      var worksheet3 = workbook.addWorksheet('Bekleyen İşlem Listesi');
      
      worksheet3.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet3.cell(1,2).date(new Date()).style(style);
      worksheet3.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet3.cell(1,5).string(fullName).style(style);
      // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
      worksheet3.cell(3,1).string("İşlem Oluşturma Tarihi").style(style1);
      worksheet3.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet3.cell(3,3).string("İşlem Türü(Alış-Satış)").style(style1);
      worksheet3.cell(3,4).string("Birim Tutarı").style(style1);
      worksheet3.cell(3,5).string("Talep(Alış-Satış) Miktar").style(style1);
      worksheet3.cell(3,6).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionBuyWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionBuyWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Alış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionBuyWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionBuyWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionBuyWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
      for(let item =0; item< data.trunsactionSellWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionSellWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionSellWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Satış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionSellWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionSellWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionSellWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
    }
    else
    {
      // alim_islemi tablosunda seller id si olan
      data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where sellerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
        data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }
      // alim_islemi tablosunda buyer id si olan
      data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where buyerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
        data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }
      // alis_listesi ve satis_listesindeki productRemainderAmount>0 ve userId olan
      data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');

      data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');
      
      var worksheet = workbook.addWorksheet('Alış İşlem Listesi');
      var worksheet2 = workbook.addWorksheet('Satış İşlem Listesi');
      var worksheet3 = workbook.addWorksheet('Bekleyen İşlem Listesi');
      // 
      
      worksheet.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet.cell(1,2).date(new Date()).style(style);
      worksheet.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet.cell(1,5).string(fullName).style(style);

      worksheet2.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet2.cell(1,2).date(new Date()).style(style);
      worksheet2.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet2.cell(1,5).string(fullName).style(style);

      worksheet3.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet3.cell(1,2).date(new Date()).style(style);
      worksheet3.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet3.cell(1,5).string(fullName).style(style);

      worksheet.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet.cell(3,3).string("Alım Birim Tutarı").style(style1);
      worksheet.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet.cell(3,5).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyProduct.length; item++)
      {
        worksheet.cell(rawNumber,1).date(data.trunsactionBuyProduct[item].transactionProcessTime).style(style);
        worksheet.cell(rawNumber,2).string(data.trunsactionBuyProduct[item].productName).style(style);
        worksheet.cell(rawNumber,3).number(data.trunsactionBuyProduct[item].productPrice).style(style);
        worksheet.cell(rawNumber,4).number(data.trunsactionBuyProduct[item].productAmount).style(style);
        worksheet.cell(rawNumber,5).string(data.trunsactionBuyProduct[item].productUnit).style(style);
        rawNumber++;
      }
      // Alış     =>  İşlem Gerçekleşme Tarihi    Ürün Tipi(Adi)     Satış Birim Tutarı     Alınan Miktar
      worksheet2.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet2.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet2.cell(3,3).string("Satış Birim Tutarı").style(style1);
      worksheet2.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet2.cell(3,5).string("Ürün Birimi").style(style1);
      rawNumber = 4;
      for(let item =0; item< data.trunsactionSellProduct.length; item++)
      {
        worksheet2.cell(rawNumber,1).date(data.trunsactionSellProduct[item].transactionProcessTime).style(style);
        worksheet2.cell(rawNumber,2).string(data.trunsactionSellProduct[item].productName).style(style);
        worksheet2.cell(rawNumber,3).number(data.trunsactionSellProduct[item].productPrice).style(style);
        worksheet2.cell(rawNumber,4).number(data.trunsactionSellProduct[item].productAmount).style(style);
        worksheet2.cell(rawNumber,5).string(data.trunsactionSellProduct[item].productUnit).style(style);
        rawNumber++;
      }
      
      // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
      worksheet3.cell(3,1).string("İşlem Oluşturma Tarihi").style(style1);
      worksheet3.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet3.cell(3,3).string("İşlem Türü(Alış-Satış)").style(style1);
      worksheet3.cell(3,4).string("Birim Tutarı").style(style1);
      worksheet3.cell(3,5).string("Talep(Alış-Satış) Miktar").style(style1);
      worksheet3.cell(3,6).string("Ürün Birimi").style(style1);
      rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionBuyWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionBuyWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Alış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionBuyWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionBuyWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionBuyWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
      for(let item =0; item< data.trunsactionSellWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionSellWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionSellWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Satış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionSellWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionSellWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionSellWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
    }
  }
  else
  {
    let selectedProductName = bodyVal.urunler;
    if(bodyVal.durum == "satis")
    {
      // alim_islemi tablosunda seller id si olan
      data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where productName="'+ selectedProductName +'" and sellerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
        data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }

      var worksheet2 = workbook.addWorksheet('Satış İşlem Listesi');
      
      worksheet2.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet2.cell(1,2).date(new Date()).style(style);
      worksheet2.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet2.cell(1,5).string(fullName).style(style);

      // Alış     =>  İşlem Gerçekleşme Tarihi    Ürün Tipi(Adi)     Satış Birim Tutarı     Alınan Miktar
      worksheet2.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet2.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet2.cell(3,3).string("Satış Birim Tutarı").style(style1);
      worksheet2.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet2.cell(3,5).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionSellProduct.length; item++)
      {
        worksheet2.cell(rawNumber,1).date(data.trunsactionSellProduct[item].transactionProcessTime).style(style);
        worksheet2.cell(rawNumber,2).string(data.trunsactionSellProduct[item].productName).style(style);
        worksheet2.cell(rawNumber,3).number(data.trunsactionSellProduct[item].productPrice).style(style);
        worksheet2.cell(rawNumber,4).number(data.trunsactionSellProduct[item].productAmount).style(style);
        worksheet2.cell(rawNumber,5).string(data.trunsactionSellProduct[item].productUnit).style(style);
        rawNumber++;
      }
      
    }
    else if(bodyVal.durum == "alis")
    {
      data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where productName="'+ selectedProductName +'" and buyerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
        data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }

      var worksheet = workbook.addWorksheet('Alış İşlem Listesi');
      worksheet.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet.cell(1,2).date(new Date()).style(style);
      worksheet.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet.cell(1,5).string(fullName).style(style);

      worksheet.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet.cell(3,3).string("Alım Birim Tutarı").style(style1);
      worksheet.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet.cell(3,5).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyProduct.length; item++)
      {
        worksheet.cell(rawNumber,1).date(data.trunsactionBuyProduct[item].transactionProcessTime).style(style);
        worksheet.cell(rawNumber,2).string(data.trunsactionBuyProduct[item].productName).style(style);
        worksheet.cell(rawNumber,3).number(data.trunsactionBuyProduct[item].productPrice).style(style);
        worksheet.cell(rawNumber,4).number(data.trunsactionBuyProduct[item].productAmount).style(style);
        worksheet.cell(rawNumber,5).string(data.trunsactionBuyProduct[item].productUnit).style(style);
        rawNumber++;
      }
    }
    else if(bodyVal.durum == "bekleyen")
    {

      data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');
      data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');
    
      var worksheet3 = workbook.addWorksheet('Bekleyen İşlem Listesi');
      
      worksheet3.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet3.cell(1,2).date(new Date()).style(style);
      worksheet3.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet3.cell(1,5).string(fullName).style(style);
      // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
      worksheet3.cell(3,1).string("İşlem Oluşturma Tarihi").style(style1);
      worksheet3.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet3.cell(3,3).string("İşlem Türü(Alış-Satış)").style(style1);
      worksheet3.cell(3,4).string("Birim Tutarı").style(style1);
      worksheet3.cell(3,5).string("Talep(Alış-Satış) Miktar").style(style1);
      worksheet3.cell(3,6).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionBuyWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionBuyWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Alış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionBuyWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionBuyWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionBuyWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
      for(let item =0; item< data.trunsactionSellWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionSellWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionSellWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Satış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionSellWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionSellWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionSellWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
    }
    else
    {
      data.trunsactionSellProduct = conn.query('SELECT * FROM satis_listesi where productName="'+ selectedProductName +'" and productRemainderAmount>0 and userId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      
      data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where productName="'+ selectedProductName +'" and buyerUserId=' + userId + ' and transactionProcessTime > "' + bodyVal.startDate + '" and transactionProcessTime < "' + bodyVal.endDate + '" ORDER BY transactionProcessTime DESC');
      
      for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
        data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      }
      
      data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');
      data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime > "' + bodyVal.startDate + '" and transactionCreateTime < "' + bodyVal.endDate + '" ORDER BY transactionCreateTime DESC');
      
      
      var worksheet = workbook.addWorksheet('Satis İşlem Listesi');
      var worksheet2 = workbook.addWorksheet('Alış İşlem Listesi');
      var worksheet3 = workbook.addWorksheet('Bekleyen İşlem Listesi');
      // 
      
      worksheet.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet.cell(1,2).date(new Date()).style(style);
      worksheet.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet.cell(1,5).string(fullName).style(style);

      worksheet2.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet2.cell(1,2).date(new Date()).style(style);
      worksheet2.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet2.cell(1,5).string(fullName).style(style);

      worksheet3.cell(1,1).string("Rapor Oluşturulma Tarihi").style(style1);
      worksheet3.cell(1,2).date(new Date()).style(style);
      worksheet3.cell(1,4).string("Raporu Oluşturan").style(style1);
      worksheet3.cell(1,5).string(fullName).style(style);

      worksheet.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet.cell(3,3).string("Alım Birim Tutarı").style(style1);
      worksheet.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet.cell(3,5).string("Ürün Birimi").style(style1);
      let rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyProduct.length; item++)
      {
        worksheet.cell(rawNumber,1).date(data.trunsactionBuyProduct[item].transactionProcessTime).style(style);
        worksheet.cell(rawNumber,2).string(data.trunsactionBuyProduct[item].productName).style(style);
        worksheet.cell(rawNumber,3).number(data.trunsactionBuyProduct[item].productPrice).style(style);
        worksheet.cell(rawNumber,4).number(data.trunsactionBuyProduct[item].productAmount).style(style);
        worksheet.cell(rawNumber,5).string(data.trunsactionBuyProduct[item].productUnit).style(style);
        rawNumber++;
      }
      // Alış     =>  İşlem Gerçekleşme Tarihi    Ürün Tipi(Adi)     Satış Birim Tutarı     Alınan Miktar
      worksheet2.cell(3,1).string("İşlem Gerçekleşme Tarihi").style(style1);
      worksheet2.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet2.cell(3,3).string("Satış Birim Tutarı").style(style1);
      worksheet2.cell(3,4).string("Alınan Miktar").style(style1);
      worksheet2.cell(3,5).string("Ürün Birimi").style(style1);
      rawNumber = 4;
      for(let item =0; item< data.trunsactionSellProduct.length; item++)
      {
        worksheet2.cell(rawNumber,1).date(data.trunsactionSellProduct[item].transactionProcessTime).style(style);
        worksheet2.cell(rawNumber,2).string(data.trunsactionSellProduct[item].productName).style(style);
        worksheet2.cell(rawNumber,3).number(data.trunsactionSellProduct[item].productPrice).style(style);
        worksheet2.cell(rawNumber,4).number(data.trunsactionSellProduct[item].productAmount).style(style);
        worksheet2.cell(rawNumber,5).string(data.trunsactionSellProduct[item].productUnit).style(style);
        rawNumber++;
      }
      
      // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
      worksheet3.cell(3,1).string("İşlem Oluşturma Tarihi").style(style1);
      worksheet3.cell(3,2).string("Ürün Tipi(Adi)").style(style1);
      worksheet3.cell(3,3).string("İşlem Türü(Alış-Satış)").style(style1);
      worksheet3.cell(3,4).string("Birim Tutarı").style(style1);
      worksheet3.cell(3,5).string("Talep(Alış-Satış) Miktar").style(style1);
      worksheet3.cell(3,6).string("Ürün Birimi").style(style1);
      rawNumber = 4;
      for(let item =0; item< data.trunsactionBuyWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionBuyWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionBuyWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Alış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionBuyWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionBuyWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionBuyWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }
      for(let item =0; item< data.trunsactionSellWaitProduct.length; item++)
      {
        // Bekleyen =>  İşlem Oluşturma Tarihi      Ürün Tipi(Adi)     İşlem Türü(Alış-Satış) Birim Tutarı    Talep(Alış-Satış) Miktarı
        worksheet3.cell(rawNumber,1).date(data.trunsactionSellWaitProduct[item].transactionCreateTime).style(style);
        worksheet3.cell(rawNumber,2).string(data.trunsactionSellWaitProduct[item].productName).style(style);
        worksheet3.cell(rawNumber,3).string("Satış").style(style);
        worksheet3.cell(rawNumber,4).number(data.trunsactionSellWaitProduct[item].productPrice).style(style);
        worksheet3.cell(rawNumber,5).number(data.trunsactionSellWaitProduct[item].productRemainderAmount).style(style);
        worksheet3.cell(rawNumber,6).string(data.trunsactionSellWaitProduct[item].productUnit).style(style);
        rawNumber++;
      }

    }
  }

  workbook.write('./public/reports/' + fullName + '.xlsx');

}

var createFile = function(bodyVal){
  // Create a new instance of a Workbook class
  console.log("bodyVal")
  console.log(bodyVal)
  var workbook = new excel.Workbook();
  let alim_islemi = conn.query('SELECT * FROM alim_islemi order by transactionCreateTime DESC');
  let alis_listesi = conn.query('SELECT * FROM alis_listesi order by transactionCreateTime DESC');
  let satis_listesi = conn.query('SELECT * FROM satis_listesi order by transactionCreateTime DESC');
  // Add Worksheets to the workbook
  var worksheet = workbook.addWorksheet('Alım Listesi');
  var worksheet2 = workbook.addWorksheet('Satış Listesi');
  var worksheet2 = workbook.addWorksheet('Gerçekleşen Listesi');
 
  // Create a reusable style
  var style = workbook.createStyle({
    font: {
      color: '#000000',
      size: 12
    },
    numberFormat: '$#,##0.00; ($#,##0.00); -'
  });

  for(let i=0; i<alis_listesi.length; i++)
  {

    // Set value of cell A1 to 100 as a number type styled with paramaters of style
    // transactionId  userId  productName  productAmount  productRemainderAmount  productUnit  productPrice  transactionCreateTime  transactionProcessTime
    worksheet.cell(1,1).number(100).style(style);
  }
  for(let i=0; i<satis_listesi.length; i++)
  {

    // Set value of cell A1 to 100 as a number type styled with paramaters of style
    // transactionId  userId  productName  productAmount  productRemainderAmount  productUnit  productPrice  transactionCreateTime  transactionProcessTime
    worksheet.cell(1,1).number(100).style(style);
  }
  for(let i=0; i<alim_islemi.length; i++)
  {

    // Set value of cell A1 to 100 as a number type styled with paramaters of style
    // transactionId  buyerUserId  sellerUserId productName   productUnit  productPrice  transactionCreateTime  transactionProcessTime
    worksheet.cell(1,1).number(100).style(style);
  }
  // Set value of cell A1 to 100 as a number type styled with paramaters of style
  worksheet.cell(1,1).number(100).style(style);

  // Set value of cell B1 to 300 as a number type styled with paramaters of style
  worksheet.cell(1,2).number(200).style(style);

  // Set value of cell C1 to a formula styled with paramaters of style
  worksheet.cell(1,3).formula('A1 + B1').style(style);

  // Set value of cell A2 to 'string' styled with paramaters of style
  worksheet.cell(2,1).string('string').style(style);

  // Set value of cell A3 to true as a boolean type styled with paramaters of style but with an adjustment to the font size.
  worksheet.cell(3,1).bool(true).style(style).style({font: {size: 14}});
  worksheet2.cell(2,1).string('string').style(style);
  workbook.write('./public/reports/' + fullName + '.xlsx');
  for(let i = 1; i<100000; i++);
}
router.post('/user-download', function (req, res, next) {
  KurGuncelle();
  
  res.download('./public/reports/' + fullName + '.xlsx');
});


/* GET home page. */
router.get('/', function (req, res, next) {
  KurGuncelle();
  if (!req.session.visitcount) {
    req.session.visitcount = 1;
  }
  else {
    req.session.visitcount++;
  }

  if (userId != -1) {
    let data = {};
    data.kurlar = kurlar;
    if (userType == 1) {
      data.type = 1;
      data.product = 3;
      data.remainder = 4;
      data.transaction = 5;
      data.fullName = fullName;

      data.totalUser = totalUser;
      data.totalMoney = totalMoney;
      data.totalConfirmMoney = totalConfirmMoney;
      data.totalNotConfirmMoney = totalNotConfirmMoney;
      data.totalProduct = totalProduct;

      res.render('admin-page', data);
    }
    else {
      data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount=0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
      data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
      data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
      data.type = 6;
      data.fullName = fullName;

      data.totalUser = totalUser;
      data.totalMoney = totalMoney;
      data.totalConfirmMoney = totalConfirmMoney;
      data.totalNotConfirmMoney = totalNotConfirmMoney;
      data.totalProduct = totalProduct;

      res.render('user-page', data);
    }
  }
  else
    res.render('login');

});


router.get('/login', function (req, res, next) {
  KurGuncelle();
  if (userId != -1) {
    //KurGuncelle();
    let data = {};
    data.kurlar = kurlar;
    if (userType == 1) {
      
      data.type = 1;
      data.product = 3;
      data.remainder = 4;
      data.transaction = 5;
      data.fullName = fullName;

      data.totalUser = totalUser;
      data.totalMoney = totalMoney;
      data.totalConfirmMoney = totalConfirmMoney;
      data.totalNotConfirmMoney = totalNotConfirmMoney;
      data.totalProduct = totalProduct;

      res.render('admin-page', data);
    }
    else if(userId == 3)
    {
      data.realisedTransaction = conn.query('SELECT * FROM alim_islemi order by transactionCreateTime DESC');
      for (var i = 0; i < data.realisedTransaction.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].sellerUserId);
        let data2 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].buyerUserId);
        data.realisedTransaction[i].sellerFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        data.realisedTransaction[i].buyerFullName = data2[0].userFirstName + " " + data2[0].userSurname;
      }


      data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userType=3')[0].userMoney;

      data.fullName = fullName;

      res.render('muhasebeci-page', data);
    }
    else {
      data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
      data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');

      data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
      data.type = 6;
      data.fullName = fullName;
      res.render('user-page', data);
    }
  }
  else {
    res.render('login');
  }

});


router.post('/login', function (req, res, next) {

  KurGuncelle();
  for(let i=0; i<100000;i++);

  let data = {};
  data.kurlar = kurlar;

  let user_login = req.body.user_login;
  let password = req.body.password;

  let password_md5 = md5(password);
  
  const loginCheck = conn.query("select userType, userId, userName, userFirstName, userSurname from kullanicilar where (userName='" + user_login + "' or userEmail='" + user_login + "') and userPassword='" + password_md5 + "'");

  if (loginCheck.length) {
    // login successed
    userType = parseInt(loginCheck[0].userType);
    userId = parseInt(loginCheck[0].userId);

    fullName = loginCheck[0].userName;

    data.type = userType;
    if (userType == 1) {
      data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0');
      data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0');
      data.type = 1;
      data.product = 3;
      data.remainder = 4;
      data.transaction = 5;
      data.fullName = fullName;


      let veri = conn.query('SELECT userId FROM kullanicilar');
      //SELECT SUM(Quantity) AS TotalItemsOrdered FROM OrderDetails;
      let total = conn.query('SELECT SUM(userMoney) as tuserMoney, SUM(userWaitMoneyTL) as tuserWaitMoneyTL,  SUM(userWaitMoneyUSD) as tuserWaitMoneyUSD,  SUM(userWaitMoneyEUR) as tuserWaitMoneyEUR,  SUM(userWaitMoneyGBP) as tuserWaitMoneyGBP FROM kullanicilar');
      totalUser = veri.length;
      totalConfirmMoney = total[0].tuserMoney;
      totalNotConfirmMoney = total[0].tuserWaitMoneyTL + 
                             total[0].tuserWaitMoneyUSD * kurlar.usdAlis + 
                             total[0].tuserWaitMoneyEUR * kurlar.eurAlis + 
                             total[0].tuserWaitMoneyGBP * kurlar.gbpAlis;
      totalMoney = totalConfirmMoney + totalNotConfirmMoney;

      totalProduct = conn.query('SELECT DISTINCT productName FROM urunler').length;
      console.log(totalUser);
      console.log(totalConfirmMoney);
      console.log(totalNotConfirmMoney);
      console.log(totalMoney);
      console.log(totalProduct);

      data.totalUser = totalUser;
      data.totalMoney = totalMoney;
      data.totalConfirmMoney = totalConfirmMoney;
      data.totalNotConfirmMoney = totalNotConfirmMoney;
      data.totalProduct = totalProduct;

      res.render('admin-page', data);
    }
    else if (userType == 2) {
      data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId);
      data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId);
      data.type = 6;
      data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
      data.fullName = fullName;
      res.render('user-page', data);
    }
    else if (userType == 3) {
      data.realisedTransaction = conn.query('SELECT * FROM alim_islemi order by transactionCreateTime DESC');
      for (var i = 0; i < data.realisedTransaction.length; i++) {
        let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].sellerUserId);
        let data2 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].buyerUserId);
        data.realisedTransaction[i].sellerFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        data.realisedTransaction[i].buyerFullName = data2[0].userFirstName + " " + data2[0].userSurname;
      }


      data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userType=3')[0].userMoney;

      data.fullName = fullName;


      res.render('muhasebeci-page', data);
    }
    else {
      res.render('login');
    }
    //req.session.success = true;
  } else {
    // login failed
    //req.session.success = false;

    console.log("olmadı");
    res.render('login');
  }


});


router.get('/logout', function (req, res, next) {

  KurGuncelle();
  userType = -1;
  userId = -1;
  fullName = "";

  
  res.render('login');

});


router.get('/change-password', function (req, res, next) {
  KurGuncelle();
  res.render('change-password');

});



router.get('/signup', function (req, res, next) {
  KurGuncelle();
  res.render('signup');
});

router.post('/signup', function (req, res, next) {
  KurGuncelle();
  let password = req.body.password;
  let password_md5 = md5(password);
  //check userName, phoneNumber, tcNo, email, 
  let checkPrivate = conn.query("SELECT * FROM kullanicilar where userEmail='" + req.body.email + "' or userPhoneNumber='" + req.body.phonenumber + "' or userTcNumber='" + req.body.tcno + "' or userName='" + req.body.username + "'");

  if (checkPrivate.length > 0) {
    console.log(checkPrivate);
    res.render('signup', { "allert": "**Ayni Kullanici Adi, TC Numarasi, Telefon Numarasi veya Email farklı bir kullaniciya kayitlidir... " });
  }
  else if (req.body.confirm_password != req.body.password) {
    res.render('signup', { "allert": "Şifreler eşleşmiyor... " });
  }
  else {
    conn.query("INSERT INTO kullanicilar (userType, userCreateTime,	userFirstName,	userSurname, userName,	userPassword,	userTcNumber,	userPhoneNumber,	userEmail,	userAddress	) \
    VALUES (2, NOW(),'" + req.body.firstname + "', '" + req.body.surname + "', '" + req.body.username + "', '" + password_md5 + "', '" + req.body.tcno + "', '" + req.body.phonenumber + "', '" + req.body.email + "', '" + req.body.address + "')");
    res.render('login');
  }

});


router.get('/product-confirm', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    
    let data = {};
    data.kurlar = kurlar;

    data.products = conn.query('SELECT * FROM urunler where  productWaitAmount>0');
    data.fullName = fullName;
    res.render('product-confirm', data);
  }
});

router.get('/muhasebeci-page', function (req, res, next) {
  KurGuncelle();
  if (userType != 3) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.realisedTransaction = conn.query('SELECT * FROM alim_islemi order by transactionCreateTime DESC');
    for (var i = 0; i < data.realisedTransaction.length; i++) {
      let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].sellerUserId);
      let data2 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].buyerUserId);
      data.realisedTransaction[i].sellerFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      data.realisedTransaction[i].buyerFullName = data2[0].userFirstName + " " + data2[0].userSurname;
    }


    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userType=3')[0].userMoney;
    data.fullName = fullName;


    res.render('muhasebeci-page', data);
  }
});

router.get('/user-page', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.type = 6;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.get('/user-add-product', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.type = 7;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.post('/user-add-product', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    let _productName = conn.query("SELECT productId, productName FROM urunler where productName = '" + req.body.name + "' and userId=" + userId);
    console.log(_productName);
    if (_productName.length > 0) {
      conn.query("UPDATE urunler SET productWaitAmount = productWaitAmount + " + req.body.amount + ", productUpdateTime= NOW() where productId =" + _productName[0].productId);
    }
    else {
      conn.query("INSERT INTO urunler (userId, productCreateTime, productUpdateTime, productName, productWaitAmount, productUnit) \
      VALUES (" + userId + ", NOW(), NOW(), '" + req.body.name + "', " + req.body.amount + ", '" + req.body.unit + "') ");
    }

    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.type = 6;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});


router.get('/user-add-money', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.type = 11;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.post('/user-add-money', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    var moneyNew = parseFloat(req.body.moneyAmount);
    if(req.body.moneyUnit == "TL") 
    {
      conn.query("UPDATE kullanicilar SET userWaitMoneyTL= userWaitMoneyTL + " + moneyNew + "  WHERE userId=" + userId);
    }
    else if(req.body.moneyUnit == "USD") 
    {
      conn.query("UPDATE kullanicilar SET userWaitMoneyUSD= userWaitMoneyUSD + " + moneyNew + "  WHERE userId=" + userId);
    }
    else if(req.body.moneyUnit == "EUR") 
    {
      conn.query("UPDATE kullanicilar SET userWaitMoneyEUR= userWaitMoneyEUR + " + moneyNew + "  WHERE userId=" + userId);
    }
    else if(req.body.moneyUnit == "GBP") 
    {
      conn.query("UPDATE kullanicilar SET userWaitMoneyGBP= userWaitMoneyGBP + " + moneyNew + "  WHERE userId=" + userId);
    }
    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.type = 6;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.get('/user-sell-product', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.productsSellable = conn.query('SELECT * FROM urunler where  productAmount>0 and userId=' + userId);
    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId);

    data.type = 8;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.post('/user-sell-product', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {

    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    conn.query("UPDATE urunler SET productUpdateTime=NOW(), productAmount=" + (req.body.productamount - req.body.product_sel_amount) + "  WHERE productId=" + req.body.productid);
    // satis listesine ekle

    data.productsSellable = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId);
    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId);
    conn.query("INSERT INTO satis_listesi (userId, productId,	productName,	productAmount, productRemainderAmount,	productUnit,	productPrice,	transactionCreateTime ) \
      VALUES (" + userId + ", " + req.body.productid + ", '" + req.body.productName + "', " + req.body.product_sel_amount + ", " + req.body.product_sel_amount + ", '" + "KG" + "', " + req.body.product_sel_price + ", NOW()) ");

    data.type = 8;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    AlimIslemi();
    res.render('user-page', data);
  }
});


router.get('/user-buy-product', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.productBuyable = conn.query('SELECT DISTINCT productName FROM urunler');
    data.productWaitBuying = conn.query('SELECT * FROM alis_listesi where productRemainderAmount > 0 and userId=' + userId);
    data.type = 9;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.post('/user-buy-product', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    let tableData = conn.query("SELECT userMoney FROM kullanicilar where userId=" + userId);
    //let bakiye = req.body.userMoney;
    //let kullaniciBakiyesi = conn.query("SELECT FROM kullanicilar where userMoney=");
    let alinacakTutar = req.body.product_buy_price*req.body.product_buy_amount;
    console.log("bakiye");
    console.log(tableData);
    let bakiye=tableData[0].userMoney;
    console.log(bakiye);
    console.log("islem");
    console.log(alinacakTutar);
    
    if(bakiye >= (alinacakTutar*1.01)){
      console.log('ismi');
      
    conn.query("INSERT INTO alis_listesi(userId,	productName,	productAmount,	productRemainderAmount,	productPrice, productUnit,	transactionCreateTime,	transactionProcessTime ) \
    VALUES (" + userId + ", '" + req.body.productName + "', " + req.body.product_buy_amount + ", '" + req.body.product_buy_amount + "', " + req.body.product_buy_price + ",'KG' , NOW(), NOW()) ");
    console.log(userId);
    console.log(req.body.productName);

    conn.query("UPDATE  kullanicilar set userMoney = userMoney -" + alinacakTutar*1.01 + " where userId=" + userId);
    AlimIslemi();
    
    }
    else { 
      data.allert = "Bakiye yetersiz...";
    }
      
    data.type = 9;

    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});


router.get('/user-transaction', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.startDate = conn.query('SELECT * FROM kullanicilar where userId='+ userId )[0].userCreateTime;
    data.endDate = Date();
    // alim_islemi tablosunda seller id si olan
    data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where sellerUserId=' + userId + ' ORDER BY transactionProcessTime DESC');
    for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
      let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
      data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
    }
    // alim_islemi tablosunda buyer id si olan
    data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where buyerUserId=' + userId +' ORDER BY transactionProcessTime DESC');
    for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
      let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
      data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
    }
    // alis_listesi ve satis_listesindeki productRemainderAmount>0 ve userId olan
    data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where productRemainderAmount>0 and userId=' + userId + ' ORDER BY transactionCreateTime DESC');
    data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0 and userId=' + userId + ' ORDER BY transactionCreateTime DESC');
    data.urunler = conn.query('SELECT productName FROM urunler where userId=' + userId);
    data.type = 10;
    data.downloadState = 0;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});

router.post('/user-filter', function (req, res, next){
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    createFile1(req.body);
    let data = {};
    data.kurlar = kurlar;
    if(req.body.urunler == "hepsi")
    {
      if(req.body.durum == "satis")
      {
        // alim_islemi tablosunda seller id si olan
        data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where sellerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
          data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
      }
      else if(req.body.durum == "alis")
      {
        // alim_islemi tablosunda buyer id si olan
        data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where buyerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
          data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
      }
      else if(req.body.durum == "bekleyen")
      {
        // alis_listesi ve satis_listesindeki productRemainderAmount>0 ve userId olan
        data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');

        data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');

      }
      else
      {
        // alim_islemi tablosunda seller id si olan
        data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where sellerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
          data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
        // alim_islemi tablosunda buyer id si olan
        data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where buyerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
          data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
        // alis_listesi ve satis_listesindeki productRemainderAmount>0 ve userId olan
        data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');

        data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where userId=' + userId + ' and productRemainderAmount>0 and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');
      }
    }
    else
    {
      let selectedProductName = req.body.urunler;
      if(req.body.durum == "satis")
      {
        // alim_islemi tablosunda seller id si olan
        data.trunsactionSellProduct = conn.query('SELECT * FROM alim_islemi where productName="'+ selectedProductName +'" and sellerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        for (var i = 0; i < data.trunsactionSellProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionSellProduct[i].buyerUserId);
          data.trunsactionSellProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
      }
      else if(req.body.durum == "alis")
      {
        data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where productName="'+ selectedProductName +'" and buyerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
          data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
      }
      else if(req.body.durum == "bekleyen")
      {

        data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');
        data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');
      }
      else
      {
        data.trunsactionSellProduct = conn.query('SELECT * FROM satis_listesi where productName="'+ selectedProductName +'" and productRemainderAmount>0 and userId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        
        data.trunsactionBuyProduct = conn.query('SELECT * FROM alim_islemi where productName="'+ selectedProductName +'" and buyerUserId=' + userId + ' and transactionProcessTime >= "' + req.body.startDate + '" and transactionProcessTime <= "' + req.body.endDate + '" ORDER BY transactionProcessTime DESC');
        
        for (var i = 0; i < data.trunsactionBuyProduct.length; i++) {
          let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.trunsactionBuyProduct[i].sellerUserId);
          data.trunsactionBuyProduct[i].userFullName = data1[0].userFirstName + " " + data1[0].userSurname;
        }
        
        data.trunsactionBuyWaitProduct = conn.query('SELECT * FROM alis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');
        data.trunsactionSellWaitProduct = conn.query('SELECT * FROM satis_listesi where productRemainderAmount>0 and productName="'+ selectedProductName +'" and userId=' + userId + ' and transactionCreateTime >= "' + req.body.startDate + '" and transactionCreateTime <= "' + req.body.endDate + '" ORDER BY transactionCreateTime DESC');
        
      }
    }
    
    data.urunler = conn.query('SELECT productName FROM urunler where userId=' + userId);

    data.type = 10;
    data.downloadState = 1;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }

});


router.post('/user-page', function (req, res, next) {
  KurGuncelle();
  if (userType != 2) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.productsConfirm = conn.query('SELECT * FROM urunler where productAmount>0 and userId=' + userId + ' ORDER BY productUpdateTime DESC');
    data.type = 6;
    data.remaindAmount = conn.query('SELECT userMoney FROM kullanicilar where userId=' + userId)[0].userMoney;
    data.fullName = fullName;
    res.render('user-page', data);
  }
});


router.get('/admin-product-confirm', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    //data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0');
    data.productsWaitConfirm = conn.query('select urun.productId, urun.productName, urun.productWaitAmount, urun.productUnit, \
    urun.productCreateTime, kullanici.userFirstName, kullanici.userSurname from urunler  \
        as urun left join kullanicilar as kullanici on kullanici.userId=urun.userId where productWaitAmount>0 \
         order by productCreateTime ASC');

    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;
    data.type = 3;    

    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});

router.post('/admin-product-confirm-ok', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.type = 1;
    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;

    conn.query("UPDATE urunler SET productUpdateTime=NOW(), productAmount = productAmount + productWaitAmount, productWaitAmount = 0 WHERE productId=" + req.body.productId);
    

    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0');

    data.type = 3;

    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});

router.post('/admin-product-confirm-no', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.type = 1;
    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;

    conn.query("UPDATE urunler SET productUpdateTime=NOW(), productRefuseAmount = productRefuseAmount + productWaitAmount, productWaitAmount = 0 WHERE productId=" + req.body.productId);

    data.productsWaitConfirm = conn.query('SELECT * FROM urunler where productWaitAmount>0');

    data.type = 3;

    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});

router.get('/admin-money-confirm', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    //data.userRemainderConfirm = conn.query('SELECT * FROM kullanicilar where userWaitMoney>0');
    data.userRemainderConfirmTL = conn.query('SELECT * FROM kullanicilar where userWaitMoneyTL>0');
    data.userRemainderConfirmUSD = conn.query('SELECT * FROM kullanicilar where userWaitMoneyUSD>0');
    data.userRemainderConfirmEUR = conn.query('SELECT * FROM kullanicilar where userWaitMoneyEUR>0');
    data.userRemainderConfirmGBP = conn.query('SELECT * FROM kullanicilar where userWaitMoneyGBP>0');
    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;
    data.type = 4;

    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});

router.post('/admin-money-confirm-ok', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;

    if(req.body.moneyUnit == "TL")
    {
      conn.query("UPDATE kullanicilar SET  userMoney= userMoney + userWaitMoneyTL,  userWaitMoneyTL=0 WHERE userId=" + req.body.userId);
    }
    else if(req.body.moneyUnit == "USD")
    {
      conn.query("UPDATE kullanicilar SET  userMoney= userMoney + userWaitMoneyUSD*" + kurlar.usdAlis + ",  userWaitMoneyUSD=0 WHERE userId=" + req.body.userId);
    }
    else if(req.body.moneyUnit == "EUR")
    {
      conn.query("UPDATE kullanicilar SET  userMoney= userMoney + userWaitMoneyEUR*" + kurlar.eurAlis + ",  userWaitMoneyEUR=0 WHERE userId=" + req.body.userId);
    }
    else if(req.body.moneyUnit == "GBP")
    {
      conn.query("UPDATE kullanicilar SET  userMoney= userMoney + userWaitMoneyGBP*" + kurlar.gbpAlis + ",  userWaitMoneyGBP=0 WHERE userId=" + req.body.userId);
    }

    data.userRemainderConfirmTL = conn.query('SELECT * FROM kullanicilar where userWaitMoneyTL>0');
    data.userRemainderConfirmUSD = conn.query('SELECT * FROM kullanicilar where userWaitMoneyUSD>0');
    data.userRemainderConfirmEUR = conn.query('SELECT * FROM kullanicilar where userWaitMoneyEUR>0');
    data.userRemainderConfirmGBP = conn.query('SELECT * FROM kullanicilar where userWaitMoneyGBP>0');

    data.type = 4;


    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});

router.post('/admin-money-confirm-no', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;
    
    if(req.body.moneyUnit == "TL")
    {
      conn.query("UPDATE kullanicilar SET userRefuseMoney= userRefuseMoney + userWaitMoneyTL,  userWaitMoneyTL=0 WHERE userId=" + req.body.userId);
    }
    else if(req.body.moneyUnit == "USD")
    {
      conn.query("UPDATE kullanicilar SET  userRefuseMoney= userRefuseMoney + userWaitMoneyUSD*" + kurlar.usdAlis + ",  userWaitMoneyUSD=0 WHERE userId=" + req.body.userId);
    }
    else if(req.body.moneyUnit == "EUR")
    {
      conn.query("UPDATE kullanicilar SET  userRefuseMoney= userRefuseMoney + userWaitMoneyEUR*" + kurlar.eurAlis + ",  userWaitMoneyEUR=0 WHERE userId=" + req.body.userId);
    }
    else if(req.body.moneyUnit == "GBP")
    {
      conn.query("UPDATE kullanicilar SET  userRefuseMoney= userRefuseMoney + userWaitMoneyGBP*" + kurlar.gbpAlis + ",  userWaitMoneyGBP=0 WHERE userId=" + req.body.userId);
    }


    data.userRemainderConfirmTL = conn.query('SELECT * FROM kullanicilar where userWaitMoneyTL>0');
    data.userRemainderConfirmUSD = conn.query('SELECT * FROM kullanicilar where userWaitMoneyUSD>0');
    data.userRemainderConfirmEUR = conn.query('SELECT * FROM kullanicilar where userWaitMoneyEUR>0');
    data.userRemainderConfirmGBP = conn.query('SELECT * FROM kullanicilar where userWaitMoneyGBP>0');

    data.type = 4;
    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});


router.get('/admin-transaction', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.realisedTransaction = conn.query('SELECT * FROM alim_islemi order by transactionCreateTime DESC');
    for (var i = 0; i < data.realisedTransaction.length; i++) {
      let data1 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].sellerUserId);
      let data2 = conn.query('SELECT userFirstName, userSurname FROM kullanicilar where userId=' + data.realisedTransaction[i].buyerUserId);
      data.realisedTransaction[i].sellerFullName = data1[0].userFirstName + " " + data1[0].userSurname;
      data.realisedTransaction[i].buyerFullName = data2[0].userFirstName + " " + data2[0].userSurname;
    }



    data.waitTransaction = conn.query('select satis.productName, satis.productRemainderAmount, satis.productUnit, satis.productPrice, satis.transactionCreateTime, kullanici.userFirstName, kullanici.userSurname from satis_listesi  as satis left join kullanicilar as kullanici on kullanici.userId=satis.userId where productRemainderAmount>0  order by transactionCreateTime DESC');


    data.type = 5;
    data.product = 3;
    data.remainder = 4;
    data.transaction = 5;
    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }


});

router.get('/admin-page', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.type = 1;
    data.product = 3;
    data.remainder = 4;
    data.transaction = 5;
    data.fullName = fullName;


    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});

router.post('/admin-page', function (req, res, next) {
  KurGuncelle();
  if (userType != 1) {
    res.render('login');
  }
  else {
    let data = {};
    data.kurlar = kurlar;
    data.type = 1;
    data.product = 1;
    data.remainder = 2;
    data.transaction = 3;

    data.fullName = fullName;

    data.totalUser = totalUser;
    data.totalMoney = totalMoney;
    data.totalConfirmMoney = totalConfirmMoney;
    data.totalNotConfirmMoney = totalNotConfirmMoney;
    data.totalProduct = totalProduct;

    res.render('admin-page', data);
  }
});


module.exports = router;
