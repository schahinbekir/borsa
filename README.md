Borsa Projesi:

*** Proje Özeti: Uygulama bir alım satım sistemidir. kullanıcıların sisteme eklediği ürünlerin ve paranın admin onayı ile kullanılabilir hale gelmesiyle alım ve satım istekleri yapılabilir hale gelir. Satış ve alış emirleri girilir. Uygun koşullar oluştuğunda kullanıcılardan; alıcı ürün alır ve para harcar, satıcı ürün satar ve para kazanır ve muhasebecimiz alıcıdan belli bir oranda komisyon alır.

Gerçekleştirme Adımları:
* İlk olarak node.js, express.js, twig, mysql, md5, excel4node, xml2js, node-fetch kurulumu yaparak projemi oluşturdum.
* Listesini tutmam gereken kullanıcılar, ürünler, satış emirleri, alış emirleri ve alım işlemleri tablolarını mysql kullanarak oluşturdum.
* Kullanıcılar tablosuna kullanıcı tipi 1 olan bir admin ekledim  ve de kullanıcı tipi 3 olan bir muhasebe kullanıcısı ekledim.
* Sisteme giriş ve kayıt: Kullanıcıdan ilk giriş için kayıt bilgilerini aldım ve kullanıcılar tablosuna ekledim. Daha sonraki girişler için ise kullanıcı adı, tc nosu veya emailini; şifresi ile beraber istedim. Admin kontrolü ve kullanıcı kontrolünü kullanıcı tipi ekleyerek ayırdım. Eşleştirme kontrolleri yaparak doğru ise kullanıcı numarası ile birlikte kullanıcı sayfasına yönlendirdim. Admin girişi olursa admin sayfasına yönlendirdim. Yanlış girişte giriş işlemini tekrarlattım.
* Sisteme ürün ekleme: Kullanıcıdan ürün ekleme emrini aldım ve bu emri ürünler tablosunda kullanıcının bekleyen ürünlerine yazdırdım. Aynı zamanda adminin onay bekleyen ürünler listesine gönderdim. Admin onayı olması halinde kullanıcının mal varlığına ürünü aktardım, reddetmesi durumunda ise tablonun reddedilen ürünlerine aktardım.
* Sisteme para ekleme: Kullanıcıdan para ekleme emrini aldım ve bu emri kullanıcılar tablosunda kullanıcının bekleyen parasına yazdırdım. Aynı zamanda adminin onay bekleyen para listesine gönderdim. Admin onayı olması halinde kullanıcının para bakiyesine parayı aktardım, reddetmesi durumunda ise tablonun reddedilen parasına aktardım.
* Ürün satış emri verme: Kullanıcıdan sahip olduğu ürünlerden satılma emirlerini aldım ve mal varlığından düşürüp satış emirleri tablosuna aktardım.
* Ürün alış emri verme: Kullanıcıdan almak istediği ürünlerin bilgisini aldım ve para bakiyesinden karşılığını ve komisyon ücretini düşürüp alış emirleri tablosuna ekledim.
* Fiyat Oluşması: Alış listesindeki emirleri satış listesi ile karşılaştırdım. Alım için alıcının istediği ürünün, belirlediği maksimum fiyat ve altındaki satış emiri varsa ucuzdan pahalıya doğru istediği miktara ulaşana kadar aldım. Eğer istenen ürün satlıkta yoksa veya daha az miktarda ürün varsa alabildiğimi aldim ve kalanını beklettim. Alıcımız ürünü daha az paraya almışsa fazla verilen parayı ve onun oranında komisyonu alıcıya iade ettim. Bu işlemi yapıp alınan ürünleri alıcının mal varlığına ekledim. alıcıdan alış işlemi kadar parayı satın aldığı kullanıcının para bakiyesine aktardım, yüzde birlik kadar da ayrıca muhasebeciye komisyon olarak ekledim. Ve alınan ürünün ne kadara ve kimlerden alındığını alış işlemleri tablosuna yazdırdım. Ayrıca alım satım işlemini admin tarafına da gösterdim.
* Döviz Girişi: Kullanıcıya TL’ nin yanında USD, EURO, GBP para birimlerini ekleme olanağı sundum ve admin onayı ile onay anında alış fiyatına göre TL ye çevirdim.
* Rapor Oluşturma: Kullanıcımıza belirli tarih aralığında alış, satış veya her ikisi birden; ve de bekleyen işlemleri bir ürün veya tüm ürünler için raporlama ve excel çıktısı oluşturma imkanı oluşturdum.


*** Nasıl İndirilir:
git yüklü bir bilgisayarda, indirmek istenilen dizine girerek
>>git clone https://bitbucket.org/schahinbekir/borsa.git 
Komutu ile proje klasörünü indiririz.
>>cd borsa
>>npm install 
->komutu ile projede daha önceden kullanılmış olan tüm kütüphaneler indirilmiş olur.

*** Kullanılan Teknolojiler: node.js, express.js, twig, mysql, md5, excel4node, xml2js, node-fetch, Visual Studio Code, Git Bash, glance_design_dashboard-web_Free admin panelinden bazı sayfalar kullanıldı.

*** Projeyi Çalıştırma Adımları: 
Node.js kurulumu yapılır. 
MYSQL kurulumu yapılır. 
Visual Studio Code kurulumu yapılır. 
Git Bash kurulumu yapılır.
Sonrasında;
Veri tabanı dosyamız MYSQL'e eklenir ve MYSQL aktif hale getirilir.
Proje dosyasının içinde mausedan sağ tık yapılarak "Git Bash Here" tıklanır.
Git Bash' da 
>>npm run start komutu ile proje çalışır.
Tarayıcıdan: localhost:5000 ile login ekranına geliriz.

Kayıtlı Admin -> Kullanıcı Adı:admin1 Şifre:Admin.11
Kayıtlı Muhasebeci -> Kullanıcı Adı:muh_kul1 Şifre:MuhKul.1



*** Projeyi Geliştirenler: Bekir Şahin
